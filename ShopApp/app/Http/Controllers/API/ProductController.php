<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Summary of index
     * 
     * @return JsonResponse|mixed
     */
    public function index()
    {
        $products = $this->product->paginate(5);

        $productResource = ProductResource::collection($products)->response()->getData(true);

        return $this->responseSuccess($productResource, 'Success', Response::HTTP_OK);
    }

    /**
     * Summary of store
     * 
     * @param StoreProductRequest $request
     * @return JsonResponse|mixed
     */
    public function store(StoreProductRequest $request)
    {
        $dataCreate = $request->all();

        $product = $this->product->create($dataCreate);

        $productResource = new ProductResource($product);

        return $this->responseSuccess($productResource, 'Created Success', Response::HTTP_CREATED);
    }

    /**
     * Summary of show
     * 
     * @param $id
     * @return JsonResponse|mixed
     */
    public function show(string $id)
    {
        $product = $this->product->findOrFail($id);

        $productResource = new ProductResource($product);

        return $this->responseSuccess($productResource, 'Success', Response::HTTP_OK);
    }

    /**
     * Summary of update
     * 
     * @param UpdateProductRequest $request
     * @param $id
     * @return JsonResponse|mixed
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        $product = $this->product->findOrFail($id);

        $updateProduct = $request->all();

        $product->update($updateProduct);

        $productResource = new ProductResource($product);

        return $this->responseSuccess($productResource, 'Updated Success', Response::HTTP_OK);
    }

    /**
     * Summary of destroy
     * 
     * @param $id
     * @return JsonResponse|mixed
     */
    public function destroy(string $id)
    {
        $product = $this->product->findOrFail($id);

        $product->delete();

        $productResource = new ProductResource($product);

        return $this->responseSuccess($productResource, 'Deleted Success', Response::HTTP_OK);
    }
}
