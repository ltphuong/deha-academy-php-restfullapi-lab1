<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function responseSuccess($data = null , $message, $status_code)
    {
        return \response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data,
        ],$status_code);
    }

}
