<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */

    public function user_can_update_product_if_product_exists_and_is_valid()
    {
        $product = Product::factory()->create();

        $dataUpdate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => '999999'
        ];

        $response = $this->json('PUT',route('products.update', $product->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn (AssertableJson $json) => 
            $json->has('data', fn(AssertableJson $json) => 
                $json->where('product_code', $dataUpdate['product_code'])
                ->where('product_name', $dataUpdate['product_name'])
                ->where('product_price', $dataUpdate['product_price'])
                ->etc()
            )->etc()  
        );
        $this->assertDatabaseHas('products', [
            'product_code' => $dataUpdate['product_code'],
            'product_name' => $dataUpdate['product_name'],
            'product_price' => $dataUpdate['product_price'],
        ]);
    }

    /** @test */

    public function user_can_not_update_product_if_product_exists_and_product_code_is_null()
    {
        $product = Product::factory()->create();
        $dataUpdate = [
            'product_code' => '',
            'product_name' => 'test',
            'product_price' => '9000',
        ];

        $response = $this->json('PUT', route('products.update', $product->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_code')
                ->etc()
            )
        );
    }

    /** @test */

    public function user_can_not_update_product_if_product_exists_and_product_name_is_null()
    {
        $product = Product::factory()->create();
        $dataUpdate = [
            'product_code' => 'abc123',
            'product_name' => '',
            'product_price' => '9000',
        ];

        $response = $this->json('PUT', route('products.update', $product->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_name')
                ->etc()
            )
        );
    }

    /** @test */

    public function user_can_not_update_product_if_product_exists_and_product_price_is_null()
    {
        $product = Product::factory()->create();
        $dataUpdate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => '',
        ];

        $response = $this->json('PUT', route('products.update', $product->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_price')
                ->etc()
            )
        );
    }

    /** @test */

    public function user_can_not_update_product_if_product_exists_and_product_price_is_not_numeric()
    {
        $product = Product::factory()->create();
        $dataUpdate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => 'test',
        ];

        $response = $this->json('PUT', route('products.update', $product->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_price')
                ->etc()
            )
        );
    }

    /** @test */

    public function user_can_not_update_product_if_product_not_exists_and_data_is_valid()
    {
        $productId = -1;

        $response = $this->json('DELETE', route('products.destroy', $productId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
