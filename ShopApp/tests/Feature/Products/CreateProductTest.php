<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */

    public function user_can_create_product_if_product_is_valid()
    {
        $dataCreate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => '80000'
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) =>
                $json->where('product_code', $dataCreate['product_code'])
                    ->where('product_name', $dataCreate['product_name'])
                    ->where('product_price', $dataCreate['product_price'])
                    ->etc()
            )->etc()
        );
    }

    /** @test */

    public function user_can_not_create_product_if_product_code_is_null()
    {
        $dataCreate = [
            'product_code' => '',
            'product_name' => 'test',
            'product_price' => '80000'
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_code')
                ->etc()
            )->etc()
        );
    }

        /** @test */

    public function user_can_not_create_product_if_product_name_is_null()
    {
        $dataCreate = [
            'product_code' => 'abc123',
            'product_name' => '',
            'product_price' => '80000'
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_name')
                ->etc()
            )->etc()
        );
    }

    /** @test */

    public function user_can_not_create_product_if_product_price_is_null()
    {
        $dataCreate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => ''
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_price')
                ->etc()
            )->etc()
        );
    }

    /** @test */

    public function user_can_not_create_product_if_product_price_is_not_numeric()
    {
        $dataCreate = [
            'product_code' => 'abc123',
            'product_name' => 'test',
            'product_price' => 'abc'
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_price')
                ->etc()
            )->etc()
        );
    }

    /** @test */

    public function user_can_not_create_product_if_data_is_not_valid()
    {
        $dataCreate = [
            'product_code' => '',
            'product_name' => '',
            'product_price' => ''
        ];

        $response = $this->json('POST', route('products.store', $dataCreate));

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'errors',
                fn(AssertableJson $json) =>
                $json->has('product_code')
                ->has('product_name')
                ->has('product_price')
                ->etc()
            )->etc()
        );
    }
}
