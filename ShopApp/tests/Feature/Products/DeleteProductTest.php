<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */

    public function user_can_delete_product_if_product_exists()
    {
        $product = Product::factory()->create();

        $productCountBeforeDelete = Product::count();

        $response = $this->json('DELETE', route('products.destroy', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) =>
                $json->where('product_code', (string) $product->product_code)
                ->where('product_name', $product->product_name)
                ->where('product_price', $product->product_price)
                
                ->etc()
            )->etc()
        );

        $productCountAfterDelete = Product::count();

        $this->assertEquals($productCountBeforeDelete - 1, $productCountAfterDelete);
    }

    /** @test */

    public function user_can_not_delete_product_if_product_not_exists()
    {
        $productId = -1;

        $response = $this->json('DELETE', route('products.destroy', $productId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

}
