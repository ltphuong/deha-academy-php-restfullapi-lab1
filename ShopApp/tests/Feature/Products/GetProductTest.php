<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test */

    public function user_can_get_product_if_product_exists()
    {
        $product = Product::factory()->create();

        $response = $this->getJson(route('products.show', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) =>
                $json->has('product_code')
                    ->has('product_name')
                    ->has('product_price')
                    ->etc()
            )
                ->has('status_code')
                ->has('message')
        );
    }

    /** @test */

    public function user_can_not_get_product_if_product_not_exists()
    {
        $productId = -1;

        $response = $this->getJson(route('products.show', $productId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
