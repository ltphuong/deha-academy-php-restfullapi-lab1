<?php

namespace Tests\Feature\Products;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */

    public function user_can_get_list_product()
    {
        $response = $this->getJson(route('products.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn(AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) => 
                $json->has('data')
                ->has('links')
                ->has('meta')
                ->etc()  
            )
            ->has('status_code')
            ->has('message')
            ->etc()
        );
    }
}
